<?php $this->load->view('header') ?>
<div class="row">
	<div class="col s12">
		<h4 class="page-title">Edit <?php echo $product->name ?></h4>
	</div>
</div>
<?php if ($this->session->flashdata('error')) { ?>
<div class="row">
	<div class="col s12">
		<div class="alert-box orange darken-4 white-text"><?php echo $this->session->flashdata('error'); ?></div>
	</div>
</div>
<?php } ?>
<?php if ($this->session->flashdata('success')) { ?>
<div class="row">
	<div class="col s12">
		<div class="alert-box green white-text"><?php echo $this->session->flashdata('success'); ?></div>
	</div>
</div>
<?php } ?>
<div class="row">
	<form class="col s12" method="POST">
		<input type="hidden" name="product_id" value="<?php echo $product->id ?>">
      	<div class="row">
	        <div class="input-field col l4 m4 s12">
	          	<input placeholder="SKU (Stock Keeping Unit)" id="sku" name="sku" type="text" class="validate" value="<?php echo $product->sku ?>">
	          	<label for="sku">SKU</label>
	        </div>
	        <div class="input-field col l8 m8 s12">
	          	<input placeholder="Name of the product" id="name" name="name" type="text" class="validate" value="<?php echo $product->name ?>">
	          	<label for="name">Product's Name</label>
	        </div>
      	</div>
      	<div class="row">
      		<div class="input-field col s12">
          		<textarea id="description" class="materialize-textarea" name="description" placeholder="Type product's description"><?php echo $product->description ?></textarea>
          		<label for="description">Description</label>
        	</div>
      	</div>
      	<div class="row">
	        <div class="input-field col s12 m6 l7">
	          	<input placeholder="Price per unit (Rp)" id="price" name="price" type="text" class="validate" value="<?php echo $product->price ?>">
	          	<label for="price">Price</label>
	        </div>
	        <div class="input-field col s12 m6 l5">
	          	<input placeholder="Tax in percent" id="tax" name="tax" type="text" class="validate" value="<?php echo $product->tax ?>">
	          	<label for="tax">Tax</label>
	        </div>
      	</div>
      	<button type="submit" class="btn waves-light waves-effect">Save Changes</button>
      	<a href="<?php echo site_url('product') ?>" class="btn waves-light waves-effect grey white-text">Back</a>
    </form>
</div>
<?php $this->load->view('footer') ?>