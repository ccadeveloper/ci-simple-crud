<?php $this->load->view('header') ?>
<div class="row">
	<div class="col s12">
		<h4 class="page-title">Products</h4>
		<a href="<?php echo site_url('product/new') ?>" class="btn waves-effect waves-light right">New Product</a>
	</div>
</div>
<?php if ($this->session->flashdata('del_error')) { ?>
<div class="row">
	<div class="col s12">
		<div class="alert-box orange darken-4 white-text"><?php echo $this->session->flashdata('del_error'); ?></div>
	</div>
</div>
<?php } ?>
<?php if ($this->session->flashdata('del_success')) { ?>
<div class="row">
	<div class="col s12">
		<div class="alert-box green white-text"><?php echo $this->session->flashdata('del_success'); ?></div>
	</div>
</div>
<?php } ?>
<div class="row">
	<div class="col s12">
		<table class="highlight bordered responsive-table">
	        <thead>
	          	<tr>
					<th data-field="inc" style="min-width: 50px" class="center">#</th>
					<th data-field="sku">SKU</th>
					<th data-field="name">Name</th>
					<th data-field="desc">Desc</th>
					<th data-field="price">Price</th>
					<th data-field="option">Option</th>
				</tr>
	        </thead>
	        <tbody>
	        <?php $i=1;foreach ($products->result() as $product_temp) { ?>
	          	<tr>
		            <td class="center"><?php echo $i++ ?></td>
		            <td><?php echo $product_temp->sku ?></td>
		            <td><?php echo $product_temp->name ?></td>
		            <td><?php echo character_limiter($product_temp->description, 40) ?></td>
		            <td><?php echo $product_temp->price ? "Rp. " . number_format($product_temp->price, 0, ",", ".") : "-" ?></td>
		            <td><a href="<?php echo site_url('product/edit/'.$product_temp->id) ?>"><i class="material-icons">edit</i></a> <a href="<?php echo site_url('product/delete/'.$product_temp->id) ?>"><i class="material-icons">close</i></a></td>
	          	</tr>
          	<?php } ?>
	        </tbody>
	    </table>
	</div>
</div>
<?php $this->load->view('footer') ?>