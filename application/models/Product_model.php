<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Product_model extends CI_Model {
	var $product_id;

	function get_products(){
		$this->db->from('products pd');
		if ($this->product_id)
			$this->db->where('pd.id', $this->product_id);

		return $this->db->get();
	}

	function get_categories(){
		$this->db->from('categories');

		return $this->db->get();
	}

	function add_product($data){
		if ($this->db->insert('products', $data))
			return TRUE;
		return FALSE;
	}

	function update_product($id, $data){
		$this->db->where('id', $id);
		if ($this->db->update('products', $data))
			return TRUE;
		return FALSE;
	}

	function remove_product(){
		$this->db->where('id', $this->product_id);
		if ($this->db->delete('products'))
			return TRUE;
		return FALSE;
	}

}

/* End of file Product_model.php */
/* Location: ./application/models/Product_model.php */